require 'mechanize'
require 'logger'

agent = Mechanize.new
agent.log = Logger.new "mech.log"
agent.user_agent_alias = 'Mac Safari'

page = agent.get "https://example.com/user" #UPDATEME with the URL of your website

# login
login_form = page.form_with :id => "user-login"
login_form.field_with(:name => "name").value = "USERNAME" #UPDATEME with the admin username
login_form.field_with(:name => "pass").value = "PASSWORD" #UPDATEME with the admin password
logged_in = agent.submit login_form

# go to admin page that lists all content
# you will need to create a view at this page
# this view should be a simple table, with edit buttons
# this view should only display nodes with an address, but no coordinates

page = agent.get "https://example.com/batch-geocode" #UPDATEME with the URL of your the view containing your nodes
edit_links = page.links_with(:text => 'edit')

# fill out each form
edit_links.each do |link|
    page = link.click

    # this is a list that corresponds to the content type of the node being edited
    
    #UPDATEME with the IDs of the node edit forms
    forms = [
        "page-node-form"
    ]
    forms.each do |form|
        begin
            node_form = page.form_with :id => form
            # field_loc[und][0][re_geocode_location] is the name of the field
            # the allows a user to re-geocode a location
            
            #UPDATEME with the name of the field that says `Re geocode `.
            # This should be displayed in the LOCATION field on your content type
            node_form.checkbox_with(:name => "field_loc[und][0][re_geocode_location]").check
            agent.submit(node_form, node_form.button_with(:value => "Save"))        
        rescue 
            puts "there was no form with an ID of #{form}"
        end
    end
end