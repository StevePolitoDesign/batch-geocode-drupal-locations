# Introduction

This is a simple `ruby` script using [Mechanize](https://github.com/sparklemotion/mechanize).

The script will batch update `nodes` that need to be re-geocoded. Specifically, this is for [Drupal](https://www.drupal.org/) sites running the [Location Module](https://www.drupal.org/project/location). 

One of the limitations of the [Location Module](https://www.drupal.org/project/location) is that [there is no way to batch geocode nodes with missing locations](https://www.drupal.org/node/1041632).

# Set Up

## 1. Create a View to Display all Nodes with an Address, but that have not been geocoded
1. Create a new view that lists all nodes with an address, but no location
    1. `/admin/structure/views/add`
    2. Set `View name` to `Batch Geocode`
    3. Set `Show` to `Content` of type `All`
    4. Select `Create a page`
    5. Set the path to `/batch-geocode`.
    6. Set the `Display format` to `table` of `fields`
    7. Click `Save & exit`
2. Navigate to the edit page for the view you just created. It should be `/admin/structure/views/view/batch_geocode/edit`
3. Under `FIELDS` click `add` and search for the `Content: Edit link` field. Select the field and click `Apply (all displays)`.
4. Under `FILTER CRITERIA` click `add` and search for the field(s) that your locations are stored in. In my case, my field is called `Location`. Select the field and click `Apply (all displays)`.
    1. Set the `Operator` to `Is not empty (NOT NULL)`, and click `Apply (all displays)`. 
5. Under `FILTER CRITERIA` click `add` and search for `Location: Latitude`. Select the field and click `Apply (all displays)`.
    1. Set the `operator` to `Is equal to` and the `value` to `0.000000`. Click `Apply (all displays)`. This will find all nodes that have not been geolocated.
6. Set the `PAGER` to `Display all items` and click `Apply (all displays)`.
7. Save the view.

## 2. Modify the Script
1. Open `script.rb`
2. Update any variable with `#UPDATEME`

## 3. Install be Dependencies
1. In the root of this project, run `$bundle install`

## 4. Run the Script
1. In the root of this project, run `$ ruby script.rb`